import pytest
import requests
import allure


class BaseRequest:
    def __init__(self, base_url):
        self.base_url = base_url
        # set headers, authorisation etc

    def _request(self, url, request_type, data=None, expected_error=False):
        stop_flag = False
        while not stop_flag:
            if request_type == 'GET':
                response = requests.get(url)
            elif request_type == 'POST':
                response = requests.post(url, json=data)
            elif request_type == 'PUT':
                response = requests.put(url, json=data)
            else:
                response = requests.delete(url)

            if not expected_error and response.status_code == 200:
                stop_flag = True
            elif expected_error:
                stop_flag = True
        return response

    def get(self, endpoint, expected_error=True):
        url = f'{self.base_url}/{endpoint}'
        with allure.step(f'GET from {url}'):
            return self._request(url, 'GET', expected_error=expected_error)

    def post(self, endpoint, body=None):
        url = f'{self.base_url}/{endpoint}'
        with allure.step(f'POST to {url}'):
            return self._request(url, 'POST', data=body, expected_error=True).json()

    def delete(self, endpoint):
        url = f'{self.base_url}/{endpoint}'
        with allure.step(f'Delete to {url}'):
            return self._request(url, 'DELETE').json()

    def put(self, endpoint, body):
        url = f'{self.base_url}/{endpoint}'
        with allure.step(f'PUT to {url}'):
            return self._request(url, 'PUT', data=body).json()


@pytest.fixture
def dog_api():
    return BaseRequest(base_url="https://dog.ceo/api")
